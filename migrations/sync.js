var models = require('../models/models')
var Promise = require('bluebird');

main = () => {
    let model_names = Object.keys(models);
    return Promise.mapSeries(model_names, (model_name) => {
        return models[model_name].sync({force : false});

    })
        .then((res)=> {
            console.log(res);
            process.exit();
        })
}

main()