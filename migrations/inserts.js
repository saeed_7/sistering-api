var models = require('../models/models');
var Promise = require('bluebird');


var msg1 = {
    id: 1,
    message: "what1",
    type: 'Q',
}

var msg2 = {
    id: 2,
    message: "what2",
    type: 'Q'

}
var choice1 = {
    id : 3,
    message: 'choice1',
    type: 'A',
}
var choice2 = {
    id : 4,
    message: 'choice2',
    type: 'A',
}

var link1 = {
    id : 1,
    origin: 1,
    destination: 3
}

var link2 = {
    id : 2,
    origin: 1,
    destination: 4
}

var link3 = {
    id : 3,
    origin: 3,
    destination:2
}


models.chats.upsert(msg1)
    .then(() => {
         return models.chats.upsert(msg2)
    })
    .then(() => {
        return models.chats.upsert(choice1)
    })
    .then(() => {
        return models.chats.upsert(choice2)
    })
    .then(() => {
        return models.relationships.upsert(link1)
    })
    .then(() => {
        return models.relationships.upsert(link2)
    })
    .then(() => {
        return models.relationships.upsert(link3)
    })
    .then(() => {
        process.exit()
     })
