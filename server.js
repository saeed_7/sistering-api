
var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');
var app = express();
var cors = require('cors');
var compression = require('compression');

var port = process.env.PORT || 3000


app.use(compression());
app.use('/static', express.static('public'));
app.use(cors());

var api = require('./routes/api')
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use('/api', api);
app.listen(port);


console.log('API server started: ' + port);
