
const Sequelize = require('sequelize');
var db = require('../database');

var chats = db.define('chats', {
    id : {
      type: Sequelize.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    message :{
      type: Sequelize.TEXT,
      allowNull: true
    },
    type: {
      type: Sequelize.TEXT,
      allowNull: false,
       /*
      Q
      A
      Y
      */
    }
}, {
    tableName: 'chats',
    timestamps: false,
})

module.exports = chats;