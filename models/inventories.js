const Sequelize = require('sequelize');
var db = require('../database');

var inventories = db.define('inventories', {
    id : {
      type: Sequelize.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    category: {
      type: Sequelize.TEXT,
      allowNull: true
    },
    need : {
      type: Sequelize.TEXT,
      allowNull: true
    },
    item : {
      type : Sequelize.TEXT,
      allowNull: true
    }

}, {
    tableName: 'inventories',
    timestamps: false,
})

module.exports = inventories;

