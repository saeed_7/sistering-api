let models = {    
    relationships : require('./relationships'),
    chats   : require('./chats'),
    botconfig  : require('./botconfig'),
    volunteer: require('./volunteer'),
    inventories: require('./inventories')
}

Object.keys(models).forEach(model => {
    if (models[model].associate){
        models = models[model].associate(models);
    }
});

module.exports =  models;