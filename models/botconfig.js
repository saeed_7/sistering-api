const Sequelize = require('sequelize');
var db = require('../database');

var botconfig = db.define('botconfig', {
    id : {
      type: Sequelize.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    background : Sequelize.TEXT,
    fontFamily : Sequelize.TEXT,
    headerBgColor: Sequelize.TEXT,
    headerFontColor: Sequelize.TEXT,
    headerFontSize: Sequelize.TEXT,
    botBubbleColor: Sequelize.TEXT,
    userBubbleColor: Sequelize.TEXT,
    userFontColor: Sequelize.TEXT,
    optionsColor: Sequelize.TEXT,
    optionsColorHover: Sequelize.TEXT,
    floatingButtonColor: Sequelize.TEXT,
    botDelay: Sequelize.TEXT,
    botAvatar: Sequelize.TEXT,

}, {
    tableName: 'botconfig',
    timestamps: false,
})

module.exports = botconfig;