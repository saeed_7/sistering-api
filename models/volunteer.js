const Sequelize = require('sequelize');
var db = require('../database');

var volunteers = db.define('volunteers', {
    id : {
      type: Sequelize.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    category: {
        type: Sequelize.TEXT,
        allowNull: true
    },
    email : {
        type :Sequelize.TEXT,
        allowNull: true
    },
    mail_list : {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: true
    }

}, {
    tableName: 'volunteers',
    timestamps: false,
})

module.exports = volunteers;