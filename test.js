var spawn = require('child_process').spawn;
var Promise = require('bluebird');
var os = require('os').platform();


function main (){


    return Promise.resolve()
            .then(()=> {

                let cmd;
                if (os == 'win32' || os == 'win64') {
                    cmd = 'npm.cmd'
                } else {
                    cmd = 'npm'
                }
       
                let recompile = new Promise((resolve, reject) => {
                   spawn(cmd, ['run-script', 'recompile-react'])            
                        .on("close", (code) =>{                            
                            console.log(code);
                            if (code != 0){
                                reject();
                            } else {
                                resolve()
                            }                          
                        })
           
                }) 
                return recompile;
            })
            .then(() => {
                console.log('done')
            })
            .catch(() => {
                
            })
}

main()



/*
          let cmd;
                  if (os == 'win32' || os == 'win64') {
                      cmd = 'npm.cmd';
                  } else {
                      cmd = 'npm';
                  }
             
                  let recompile = new Promise((resolve, reject) => {
                      spawn(cmd, ['run-script', 'recompile-react'])            
                          .on("close", (code) =>{                           
                              if (code != 0){
                                  reject('recompile fail');
                              } else {
                                 resolve();
                              }                          
                          })    
                    }) ;
          
                  return recompile;

                  */