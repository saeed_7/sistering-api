module.exports = {

    constructSteps: (chats, relationships) => {
        
        let questions = {};
        let choices = {};
        let combined_choices = {};        
        
        chats.forEach(chat => {

            if (chat.type == 'Q') {             
                let msg = {};
                msg['id'] = chat.id;
                msg['message'] = chat.message;
                msg['trigger'] = null;    
                questions[chat.id] = msg;        
            }
            else if (chat.type == 'A') {
                let choice = {};
                choice.label = chat.message;
                choice.trigger = chat.trigger;
                choice['value'] = null;
                choices[chat.id] = choice;

            } else if (chat.type == 'Y' ){
                let msg = {};
                msg['id'] = chat.id;
                msg['message'] = chat.message;
                msg['trigger'] = null;  
                msg['iframe'] = true;
                questions[chat.id] = msg;
            }

        });

        relationships.forEach(relationship => {
            if (choices[relationship.origin]){
                choices[relationship.origin].trigger = relationship.destination;
            }
        })

        relationships.forEach(relationship => {
            if (questions[relationship.origin]){
                if (questions[relationship.origin].trigger == null){
                    questions[relationship.origin].trigger = relationship.destination;

                    let choice_obj = {
                        "id": relationship.destination,
                        "options": []
                    }

                    let choice = choices[relationship.destination];
                    choice['value'] = 1;

                    choice_obj.options.push(choice);

                    combined_choices[relationship.destination] = choice_obj;

                } else {
                    let trigger = questions[relationship.origin].trigger;
                    let choice = choices[relationship.destination];
                    choice['value'] = combined_choices[trigger].options.length + 1;

                    combined_choices[trigger].options.push(choice);
                }
            }
        })

        let steps = {
            steps : []
        }

        Object.keys(questions).forEach(question => {
            if (question.trigger == null){
                delete question.trigger;
                question.end = true;
            }
            steps.steps.push(questions[question]);
        })

        Object.keys(combined_choices).forEach(combined_choice => {
            steps.steps.push(combined_choices[combined_choice]);
        })  
        
        return steps;
    },

    constructConfig: (configs) => {

        let result = {
            botconfig : { },
            theme : { }
        };

        configs.forEach(config => {

            if (config == 'botAvatar' || config == 'botDelay'){
                result.botconfig.push(config);
            } else {
                result.theme.push(config);
            }

        });

        return result;
    }



}