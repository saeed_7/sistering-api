var helpers = require('./helpers');



var chats = [ 
    { id: 1, message: 'what1', type: 'Q' },
    { id: 2, message: 'what2', type: 'Q' },
    { id: 3, message: 'choice1', type: 'A' },
    { id: 4, message: 'choice2', type: 'A' } ]


var relationships = [ 
    { id: 1, origin: 1, destination: 3 },
    { id: 2, origin: 1, destination: 4 },
    { id: 3, origin: 3, destination: 2 } ]

 var steps  = helpers.constructSteps(chats, relationships);

 console.log(JSON.stringify(steps));