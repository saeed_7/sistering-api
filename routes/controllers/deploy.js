
module.exports = {

    registerRoutes: (router, models, token) => {

        var helpers = require('../helpers');
        var chats = models.chats;
        var relationships = models.relationships;       
        var fs = require('fs');
        var webpack = require('webpack');
        var Promise = require('bluebird');
        var path = require('path');
        var UglifyJsPlugin = require('uglifyjs-webpack-plugin');

        router.get('/deploy', (req, res) => {
            /*
            if (!token.isValid(req.headers["x-auth"])) {
                return res.status(401).send('unauthorized');
            }
            */


            var allChats;
            var allRelationships;          

            return chats.findAll({raw:true})           
                .then((chat_response) =>{
                    allChats = chat_response;                     
                    return relationships.findAll({raw:true});                       
                })
                .then((relationships_response) => { 
                    allRelationships = relationships_response;

                    let steps = helpers.constructSteps(allChats, allRelationships);
                                       
                    fs.writeFileSync('./src/components/data/questions.json', JSON.stringify(steps));

                    var compiler = webpack ({
                        entry: path.resolve(__dirname, '../../src/main.jsx'),
                        output: {
                          path: path.resolve(__dirname, '../../public'),
                          filename: 'SisteringBot.js',
                          publicPath: path.resolve(__dirname, 'public'),
                          library: 'SisteringBot',
                          libraryTarget: 'umd',
                        },
                        resolve: {
                          extensions: ['.js', '.jsx'],
                        },
                        plugins: [
                          new UglifyJsPlugin({
                            comments: false,
                          }),
                        ],
                        module: {
                          rules: [
                            {
                              test: /\.jsx?$/,
                              exclude: /(node_modules|bower_components)/,
                              use: ['babel-loader'],
                            },
                          ],
                        },
                      });
                    
                    var run = new Promise ( (resolve, reject) => {
                        compiler.run(( err, stats) =>{
                           // console.log(stats)
                            Promise.resolve()
                            if (err){
                               // console.log(err)
                                Promise.reject();
                            }
                        })
                    })

                    return run;
                })         

                .then(() => {
                    return res.status(200).send('ok');
                })
                .catch((error) => {
                    return res.status(500).send(error);
                })          
        });

        return router;
    }
}