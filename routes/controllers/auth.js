module.exports = {
    
        registerRoutes: (router, models, token) => {
    
            var users= models.users;
            var validator = require('validator');

            router.post('/auth', (req, res) => {
                //root user change pw later
                if (req.body.username == "sysadmin" && req.body.password == 'absaefan34asdf1SD@!'){
                    let new_token = token.generate('admin');
                    return res.status(200).send(new_token)
                } else {
                    users.find({where: {username: req.body.username, password: req.body.password }})
                        .then (user =>{
                            if(user){
                                let new_token = token.generate(user.role);
                                return res.status(200).send(new_token)
                            } else {
                                return res.status(401).send('unauthorized');
                            }
                        })
                        .catch(()=>{
                            return res.status(500).send('server error');
                        })
                }
            });

            router.post('/auth/adduser', (req, res) => {
                
                if (!token.isValid(req.headers["x-auth"])) {
                    return res.status(401).send('unauthorized');
                }

                if (!validator.isLength(req.body.username, {min: 1}) || !validator.isLength(req.body.password, {min: 1})){
                    return res.status(400).send('Bad request');
                }

                user.upsert({ username: req.body.username, password: req.body.password})
                    .then(() => {
                        return res.status(200).send('ok');
                    })
                    .catch(()=>{
                        return res.status(500).send('error');
                    })
            


            });

            router.delete('/auth', (req, res) => {
                if (!token.isValid(req.headers["x-auth"])) {
                    return res.status(401).send('unauthorized');
                }
                
                if (!validator.isLength(req.body.username, {min: 1}) || !validator.isLength(req.body.password, {min: 1})){
                    return res.status(400).send('Bad request');
                }

                user.upsert({ username: req.body.username, password: req.body.password})
                    .then(() => {
                        return res.status(200).send('ok');
                    })
                    .catch(()=>{
                         return res.status(500).send('error');
                    })
                

            });    
    
    
            return router;
        }
    }