module.exports = {

    registerRoutes: (router, models, token) => {
        var relationships = models.relationships;
        router.get('/relationships', (req, res) => {
            /*
            if (!token.isValid(req.headers["x-auth"])) {
               return res.status(401).send('unauthorized');
            }
            */
            return relationships.findAll()          
                .then(result =>{    
                    res.status(200).json(result);
                }) 
                
        });

        router.delete('/relationships/:id', (req, res) => {
            /*
            if (!token.isValid(req.headers["x-auth"])) {
                return res.status(401).send('unauthorized');
            }
            */
            return relationships.destroy({where: {id: req.params.id}})          
                .then(result =>{    
                    return res.status(200).send('ok');
                }) 
                .catch(error =>{
                    return res.status(500).send('error');
                })
                
        });

        router.post('/relationships/upsert', (req, res) => {
            /*
            if (!token.isValid(req.headers["x-auth"])) {
                return res.status(401).send('unauthorized');
            }
            */
            
            if (!req.body.id){
                return relationships.create(req.body)
                    .then((created) => {
                        return res.status(200).json(created);
                    })
                    .catch((e) => {
                        return res.status(500).send('error');
                    })
            } else {
                return relationships.update(req.body, { where: { id : req.body.id } })
                    .then(() => {
                        return  res.status(200).json(req.body);
                    })
                    .catch((e) => {
                        return res.status(500).send('error');
                    })
            }

        });



        return router;
    },
    
}